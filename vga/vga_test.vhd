library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity vga_test is
port(		CLK12M 	 	: in std_logic;
--			RESET_BTN 	: in std_logic;
			FPGA_BTN  	: in std_logic;
			BOARD_BTN1 	: in std_logic;
			BOARD_BTN2 	: in std_logic;
			BOARD_BTN3 	: in std_logic;
			BOARD_BTN4 	: in std_logic;
			BOARD_BTN5 	: in std_logic;
			RED		 	: out std_logic_vector(3 downto 0);
			GREEN		 	: out std_logic_vector(3 downto 0);
			BLUE		 	: out std_logic_vector(3 downto 0);
			HS				: out std_logic;
			VS 			: out std_logic
);
end vga_test;

architecture a of vga_test is
component vga_out is
port(
	hsync	: out std_logic;
	vsync : out std_logic;
	pclock : in std_logic;
	pixel_in : in std_logic_vector(7 downto 0);
	r_out : out std_logic_vector(3 downto 0);
	g_out : out std_logic_vector(3 downto 0);
	b_out : out std_logic_vector(3 downto 0)
);
end component;
component vga_pll IS
	PORT
	(
		areset		: IN STD_LOGIC  := '0';
		inclk0		: IN STD_LOGIC  := '0';
		c0		: OUT STD_LOGIC 
	);
end component;
signal pclock : std_logic;
signal pixel : std_logic_vector(7 downto 0);
signal nreset : std_logic;
begin
pll: vga_pll port map (
	inclk0 => CLK12M,
	c0 => pclock
);
vga: vga_out port map (
	hsync => HS,
	vsync => VS,
	pclock => pclock,
	pixel_in => pixel,
	r_out => RED,
	g_out => GREEN,
	b_out => BLUE
	);
	-- TODO add reset to vga_out and connect
	nreset <= not FPGA_BTN;
	pixel <= x"FF";
end architecture a;
