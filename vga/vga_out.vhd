library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity vga_out is
generic(
	numInCol : natural := 320;
	numInRow : natural := 240
);
port(
	hsync	  : out std_logic;
	vsync   : out std_logic;
	display_time : out std_logic;
	pclock  : in std_logic;
	col_adr : out natural range 0 to numInCol-1;
	row_adr : out natural range 0 to numInRow-1
);
end vga_out;

architecture test of vga_out is

component counter
	generic (
		MAX_COUNT : natural;
		MIN_COUNT : natural);
	port (
		clk		  : in std_logic;
		reset	  : in std_logic;
		enable	  : in std_logic;
		q		  : out integer range MIN_COUNT to MAX_COUNT
	);
end component;
signal vcount : natural;
signal vreset : std_logic;
signal hreset : std_logic;
signal hcount : natural; 
begin
	h_counter: counter
		generic map ( MAX_COUNT => 800, MIN_COUNT => 0) 
		port map ( clk => pclock, reset => hreset, enable => '1', q => hcount);
	v_counter: counter
		generic map ( MAX_COUNT => 521, MIN_COUNT => 0) 
		port map ( clk => pclock, reset => vreset, enable => hreset, q => vcount);
	
	process(hcount)
	begin
		if hcount = 800 then
			hreset <= '1';
		else
			hreset <= '0';
		end if;
	end process;
	
	process(vcount)
	begin
		if vcount = 521 then
			vreset <= '1';
		else
			vreset <= '0';
		end if;
	end process;
	
	genSync: process(pclock)
	begin
		if hcount >= 0 and hcount < 96 then
			-- hsync pulse
			hsync <= '0';
		else
		   -- back porch, display time and front porch
			hsync <= '1';
		end if;
		if vcount >= 0 and vcount < 2 then
			-- vsync pulse
			vsync <= '0';
		else
			--- back porch, display time and front porch
			vsync <= '1';
		end if;
	end process;
	
	pixelAdr: process(hcount, vcount)
	begin
		if hcount >= 144 and hcount < 784 and vcount >= 31 and vcount < 511 then
			col_adr <= ( hcount - 144 ) / 2;
			row_adr <= ( vcount - 31 ) / 2;
			display_time <= '1';
			-- display time, send the pixels
			-- only grayscale for now
			-- How to use all 8 bits
--			if vcount >= 31 and vcount < 191 then
--				r_out <= pixel_in(3 downto 0);
--				g_out <= x"0";
--				b_out <= x"0";
--			elsif vcount >= 191 and vcount < 351 then
--				r_out <= x"0";
--				g_out <= pixel_in(3 downto 0);
--				b_out <= x"0";
--			else
--				r_out <= x"0";
--				g_out <= x"0";
--				b_out <= pixel_in(3 downto 0);
--			end if;
		else
			display_time <= '0';
		end if;
	end process;
end test;
