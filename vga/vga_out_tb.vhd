library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use std.textio.all;

entity out_tb is
end entity;

architecture a of out_tb is
	component vga_out is
	port(
		hsync	: out std_logic;
		vsync : out std_logic;
		pclock : in std_logic;
		pixel_in : in std_logic_vector(7 downto 0);
		r_out : out std_logic_vector(3 downto 0);
		g_out : out std_logic_vector(3 downto 0);
		b_out : out std_logic_vector(3 downto 0)
	);
	end component;
	signal clock : std_logic;
	signal hs : std_logic;
	signal vs : std_logic;
	signal pixel_out : std_logic_vector (11 downto 0);
	signal pixel_in : std_logic_vector (7 downto 0);
	-- 25 Mhz Pixel clock for 640x480@60Hz
	constant ckTime: time := 40 ns;
begin
	vga: vga_out port map (
		hsync => hs,
		vsync => vs,
		pclock => clock,
		pixel_in => pixel_in,
		r_out => pixel_out(3 downto 0),
		g_out => pixel_out(7 downto 4),
		b_out => pixel_out(11 downto 8)
	);
	-- generate clock and pixels
	clkProc: process
	begin
		clock <= '0';
		wait for ckTime/2;
		clock <= '1';
		wait for ckTime/2;
	end process;
	pixel_in <= x"FF";
end a;