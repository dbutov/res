-- Quartus Prime VHDL Template
-- Single-Port ROM

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity color_lut is

	generic 
	(
		DATA_WIDTH : natural := 8;
		ADDR_WIDTH : natural := 8
	);

	port 
	(
		clk		: in std_logic;
		addr	: in natural range 0 to 2**ADDR_WIDTH - 1;
		q		: out std_logic_vector((DATA_WIDTH -1) downto 0)
	);

end entity;

architecture rtl of color_lut is

	-- Build a 2-D array type for the ROM
	subtype word_t is std_logic_vector((DATA_WIDTH-1) downto 0);
	type memory_t is array(2**ADDR_WIDTH-1 downto 0) of word_t;

	function init_rom
		return memory_t is 
		variable tmp : memory_t := (others => (others => '0'));
	begin
	   -- init rom according to Kindlmann color scale
		--- see folder color_scheming in repository root
		tmp(0) := "000000000000";
		tmp(1) := "000100000001";
		tmp(2) := "000100000001";
		tmp(3) := "001000000010";
		tmp(4) := "001000000011";
		tmp(5) := "001000000100";
		tmp(6) := "001000000101";
		tmp(7) := "001000000101";
		tmp(8) := "001000000110";
		tmp(9) := "001000000111";
		tmp(10) := "000000001001";
		tmp(11) := "000000011000";
		tmp(12) := "000000100110";
		tmp(13) := "000000110101";
		tmp(14) := "000000110101";
		tmp(15) := "000000110100";
		tmp(16) := "000001000100";
		tmp(17) := "000001000011";
		tmp(18) := "000001000011";
		tmp(19) := "000001010011";
		tmp(20) := "000001010010";
		tmp(21) := "000001010010";
		tmp(22) := "000001010001";
		tmp(23) := "000001100001";
		tmp(24) := "000001100000";
		tmp(25) := "000001100000";
		tmp(26) := "000001110000";
		tmp(27) := "001001110000";
		tmp(28) := "001101110000";
		tmp(29) := "010001110000";
		tmp(30) := "010101110000";
		tmp(31) := "011001110000";
		tmp(32) := "011101110000";
		tmp(33) := "100001110000";
		tmp(34) := "100101110000";
		tmp(35) := "101101110000";
		tmp(36) := "110101100000";
		tmp(37) := "111001010000";
		tmp(38) := "111101010010";
		tmp(39) := "111101100100";
		tmp(40) := "111101100100";
		tmp(41) := "111101110110";
		tmp(42) := "111101110111";
		tmp(43) := "111101111001";
		tmp(44) := "111110001010";
		tmp(45) := "111110001011";
		tmp(46) := "111110001101";
		tmp(47) := "111110011110";
		tmp(48) := "111110011111";
		tmp(49) := "111110101111";
		tmp(50) := "111010101111";
		tmp(51) := "111010111111";
		tmp(52) := "111010111111";
		tmp(53) := "111011001111";
		tmp(54) := "111011001111";
		tmp(55) := "111011011111";
		tmp(56) := "111011011111";
		tmp(57) := "111011011111";
		tmp(58) := "111011101111";
		tmp(59) := "111011101111";
		tmp(60) := "111011101111";
		tmp(61) := "111011111111";
		tmp(62) := "111111111111";
		tmp(63) := "111111111111";
		return tmp;
	end init_rom;	 

	-- Declare the ROM signal and specify a default value.	Quartus Prime
	-- will create a memory initialization file (.mif) based on the 
	-- default value.
	signal rom : memory_t := init_rom;
begin

	process(clk)
	begin
	if(rising_edge(clk)) then
		q <= rom(addr);
	end if;
	end process;

end rtl;
