library ieee;
use ieee.std_logic_1164.all;
use work.fff_types_pack.all;

entity debounce_stateLogic is
	Port ( clk : in STD_LOGIC; rst : in STD_LOGIC; currentState : out debouncer_type; nextState : in debouncer_type);
end debounce_stateLogic;
architecture Behavioral of debounce_stateLogic is
signal internal_currentState: debouncer_type;
begin
sProc: process
begin
	wait until rising_edge(clk);
	if rst = '1' then
		currentState <= s0;
		internal_currentState <= s0;
	else
		-- handle forbidden jumps, that occur because the currentState signal seems to have a delay
		if internal_currentState = s2 and nextState = s0 then
			currentState <= s3;
			internal_currentState <= s3;
		elsif internal_currentState = s0 and nextState = s2 then
			currentState <= s1;
			internal_currentState <= s1;
		else
			-- handle standard jumps
			currentState <= nextState;
			internal_currentState <= nextState;
		end if;
	end if;
end process;
end Behavioral;
