library ieee;
use ieee.std_logic_1164.all;
use work.fff_types_pack.all;

entity debounce_outputLogic is
	Port (currentState : in debouncer_type; output_sig : out STD_LOGIC);
end debounce_outputLogic;
architecture Behavioral of debounce_outputLogic is
begin

yProc: process(currentState)
begin
case currentState is
	when s0 =>
		output_sig <= '0';
	when s1 =>
		output_sig <= '0';
	when s2 =>
		output_sig <= '1';
	when s3 =>
		output_sig <= '1';
	when others =>
		output_sig <= 'X'; -- default
	end case;
end process;

end Behavioral;
