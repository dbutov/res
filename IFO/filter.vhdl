library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use work.fff_types_pack.all;

entity filter3x3 is 
	port (
			clk, enable	: in std_logic;
			filterInput : in pixel3x3_type;
			mask 			: in maskS5bit3x3_type;
			outPixel 	: out std_logic_vector(7 downto 0)
	);
end entity;

architecture filter3x3_a of filter3x3 is
	
	type interimResArray is array (natural range <>) of signed(14 downto 0);
	signal weightedValues 	: interimResArray(0 to 8);
	signal sums5			 	: interimResArray(0 to 4);
	signal sums3			 	: interimResArray(0 to 2);
	signal sums2			 	: interimResArray(0 to 1);
	signal sum, sumAbs		: signed(14 downto 0);

begin
	
	genMultiply : for i in 0 to 8 generate		
		process begin
			wait until rising_edge(clk);
				weightedValues(i) <= signed( mask(i/3)(i mod 3) ) * signed( '0' & filterInput(i/3)(i mod 3) );
		end process;
	end generate genMultiply;
	
	fistAdd : for i in 0 to 3 generate
		process begin
			wait until rising_edge(clk);
				sums5(i) <= weightedValues(i*2) + weightedValues(i*2+1);
		end process;
	end generate fistAdd;
	
	process begin
		wait until rising_edge(clk);
			sums5(4) <= weightedValues(8);
	end process;	
	
	secondAdd:
	process begin
		wait until rising_edge(clk);
			sums3(0) <= sums5(0) + sums5(1);
			sums3(1) <= sums5(2) + sums5(3);
			sums3(2) <= sums5(4);
	end process;
	
	thirdAdd:
	process begin
		wait until rising_edge(clk);
			sums2(0) <= sums3(0) + sums3(1);
			sums2(1) <= sums3(2);
	end process;
	
	-- THIRD ADD
	process begin
		wait until rising_edge(clk);
			sum <= sums2(0) + sums2(1);
	end process;
		
	sumAbs <= abs(sum);
	outPixel <= std_logic_vector( sumAbs(12 downto 5) ); 
 
end filter3x3_a;




library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use work.fff_types_pack.all;

entity maskContainer is
	port(
		filterChanged 	: in std_logic := '0';
		filter			: in filter_type;
		mask				: out maskS5bit3x3_type
	);
end entity;

architecture maskContainer_a of maskContainer is
begin
	process begin
		wait until falling_edge(filterChanged);
			case filter is 
		
				when gaussFilter => 
					mask(0)(0) <= to_signed(2,6);
					mask(0)(1) <= to_signed(4,6);
					mask(0)(2) <= to_signed(2,6);
					mask(1)(0) <= to_signed(4,6);
					mask(1)(1) <= to_signed(8,6);
					mask(1)(2) <= to_signed(4,6);
					mask(2)(0) <= to_signed(2,6);
					mask(2)(1) <= to_signed(4,6);
					mask(2)(2) <= to_signed(2,6);					
					
				when scharrFilterX =>
					mask(0)(0) <= to_signed(-3,6);
					mask(0)(1) <= to_signed(0,6);
					mask(0)(2) <= to_signed(3,6);
					mask(1)(0) <= to_signed(-10,6);
					mask(1)(1) <= to_signed(0,6);
					mask(1)(2) <= to_signed(10,6);
					mask(2)(0) <= to_signed(-3,6);
					mask(2)(1) <= to_signed(0,6);
					mask(2)(2) <= to_signed(3,6);		
				
				when scharrFilterY => 
					mask(0)(0) <= to_signed(-3,6);
					mask(0)(1) <= to_signed(-10,6);
					mask(0)(2) <= to_signed(-3,6);
					mask(1)(0) <= to_signed(0,6);
					mask(1)(1) <= to_signed(0,6);
					mask(1)(2) <= to_signed(0,6);
					mask(2)(0) <= to_signed(3,6);
					mask(2)(1) <= to_signed(10,6);
					mask(2)(2) <= to_signed(3,6);
				when idFilter =>
					mask(0)(0) <= to_signed(0,6);
					mask(0)(1) <= to_signed(0,6);
					mask(0)(2) <= to_signed(0,6);
					mask(1)(0) <= to_signed(0,6);
					mask(1)(1) <= to_signed(31,6);
					mask(1)(2) <= to_signed(0,6);
					mask(2)(0) <= to_signed(0,6);
					mask(2)(1) <= to_signed(0,6);
					mask(2)(2) <= to_signed(0,6);
				
				
			end case;
	end process;

end maskContainer_a;