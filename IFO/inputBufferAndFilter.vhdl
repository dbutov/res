library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

entity setAddresToWrite is 
	generic( numCol, numRow, wDelay : natural);
	
	port(
		clk				 	: in std_logic;
		rowStart, reset 	: in std_logic:= '0';
		wEnable				: out std_logic:= '0';
		wColAddr				: out natural range 0 to numCol-1 :=0;
		wRowAddr				: out natural range 0 to numRow-1 :=0
	);
end entity;

architecture setAddresToWrite_a of setAddresToWrite is

	signal colAddr		: natural range 0 to numCol-1 :=0;
	signal rowAddr		: natural range 0 to numRow-1 :=0;

	signal wEn, wSkip : std_logic:= '0';
	signal wDelayCnt	: natural range 0 to wDelay-1;

begin

	wEnable 	<= wEn;
	wColAddr <= colAddr;
	wRowAddr <= rowAddr;
	
	
	process 
	begin
		wait until rising_edge(clk);
		if reset = '1' then
			ColAddr <= 0;
			RowAddr <= 0;
		
			wEn 	<= '0';
			wDelayCnt <= 0;
		else
			case wEn is	
				when '0' =>
				
					case wDelayCnt is 
					
						when 0 =>
							if rowStart='1' then wDelayCnt <=1; end if;
						when wDelay-1 =>	
							wEn <= '1';
							wDelayCnt <= 0;
						when others =>
							wDelayCnt <= wDelayCnt+1;
					end case;
				
				when '1' =>
				
					case colAddr is 
					
						when numCol-1 =>
							colAddr 	<= 0;
							wEn 		<= '0';
							
							if rowAddr = numRow-1 then 
								rowAddr <= 0;
							else
								rowAddr <= rowAddr+1;
							end if;
								
						when others => colAddr <= colAddr+1;
					end case;
				when others => wEn <= '0';
			end case;
		end if;
	end process;

end setAddresToWrite_a;


library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use work.fff_types_pack.all;
use work.all;

entity inputBufferAndFilter is
	
	generic(numCol, numRow : natural);
	port	( 
				clk				: in 	std_logic;
				hRef				: in 	std_logic;
				vSync				: in 	std_logic;
				inPixel			: in 	std_logic_vector(7 downto 0);
				filterChanged	: in 	std_logic;
				filterState		: in 	filter_type;
				validOutput 	: out std_logic;
				wColAddr			: out natural range 0 to numCol-1 :=0;
				wRowAddr			: out natural range 0 to numRow-1 :=0;
				wData 			: out std_logic_vector(5 downto 0)
			);
end entity;	

architecture inputBufferAndFilter_a of inputBufferAndFilter is



	constant wDelay	: natural := 5;	
	
	signal filterMask : maskS5bit3x3_type;
	
	signal window 		: pixel3x3_type; 

	signal filteredPixel : std_logic_vector(7 downto 0);
	
	signal hRefInternal : std_logic;
	
begin

	setMask : entity work.maskContainer port map(filterChanged, filterState, filterMask);
	
	inputBuffer: entity work.buffer3Rows 
			generic map(numCol, numRow)
			port map(clk, hRef, vSync, inPixel, hRefInternal, window);
	
	filter: entity work.filter3x3 port map(clk, hRef, window, filterMask, filteredPixel);
	
	setWAddres: entity setAddresToWrite 
		generic map ( numCol, numRow, wDelay)
		port map(clk, hRefInternal, vSync, validOutput, wColAddr, wRowAddr);
		
	wData 	<= filteredPixel(7 downto 2);
	
end inputBufferAndFilter_a;
