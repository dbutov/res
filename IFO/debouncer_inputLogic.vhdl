library ieee;
use ieee.std_logic_1164.all;
use work.fff_types_pack.all;

entity debounce_inputLogic is
	Port (clk : in STD_LOGIC; input_sig : in STD_LOGIC; currentState : in debouncer_type; nextState : out debouncer_type);
end debounce_inputLogic;
architecture Behavioral of debounce_inputLogic is
signal exposed_state: integer range 0 to 5;
Begin

xProc: process--(clk, input_sig, currentState)
	variable input_sig_counter: integer := 0;
	variable input_sig_counter_max: integer := 100;
begin
	wait until rising_edge(clk);	
	--nextState <= currentState; -- default
	case currentState is
		when s0 =>
			input_sig_counter := 0;
			if input_sig = '1' then 
				nextState <= s1;
				exposed_state <= 1;
			end if;
		when s1 =>
			if input_sig = '0' then 
				nextState <= s0;
				input_sig_counter := 0;
				exposed_state <= 0;
			elsif input_sig = '1' then
				input_sig_counter := input_sig_counter + 1;
				if input_sig_counter >= input_sig_counter_max then
					nextState <= s2;
					input_sig_counter := 0;
					exposed_state <= 2;
				end if;
			end if;
		when s2 =>
			input_sig_counter := 0;
			if input_sig = '0' then 
				nextState <= s3;
				exposed_state <= 3;
			end if;
		when s3 =>
			if input_sig = '1' then
				nextState <= s2;
				exposed_state <= 2;
				input_sig_counter := 0;
			elsif input_sig = '0' then
				input_sig_counter := input_sig_counter + 1;
				if input_sig_counter >= input_sig_counter_max then
					nextState <= s0;
					input_sig_counter := 0;
					exposed_state <= 0;
				end if;
			end if;
	end case;
end process;

end Behavioral;
