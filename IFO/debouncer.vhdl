library ieee;
use ieee.std_logic_1164.all;
use work.fff_types_pack.all;

Entity debouncer is
	Port (clk : in STD_LOGIC; rst : in STD_LOGIC; input_sig : in STD_LOGIC; output_sig : out STD_LOGIC);
End entity;

Architecture a1 of debouncer is
signal currentState_g: debouncer_type;
signal nextState_g: debouncer_type;
-- define components
component debounce_inputLogic
	port(clk : in STD_LOGIC; input_sig : in STD_LOGIC; currentState : in debouncer_type; nextState : out debouncer_type);
end component;
component debounce_stateLogic
	port(clk : in STD_LOGIC; rst : in STD_LOGIC; currentState : out debouncer_type; nextState : in debouncer_type);
end component;
component debounce_outputLogic
	port(currentState : in debouncer_type; output_sig : out STD_LOGIC);
end component;

Begin
	-- Putting all together ...
	part_input: debounce_inputLogic port map (clk, input_sig, currentState_g, nextState_g);
	part_state: debounce_stateLogic port map (clk, rst, currentState_g, nextState_g);
	part_output: debounce_outputLogic port map (currentState_g, output_sig);
end a1;
