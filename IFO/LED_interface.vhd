library ieee;
use ieee.std_logic_1164.all;
use work.fff_types_pack.all;

entity LED_INTERFACE is
	generic(vga_vsync_led, cam_vsync_led, cam_config_led, gauss_led, scharrX_led, scharrY_led, id_led, pwm_percentage_on: natural);
	Port (
		CLK: in STD_LOGIC;
		VGA_VSYNC: in STD_LOGIC;
		CAM_VSYNC: in STD_LOGIC;
		SCCB_FINISHED: in STD_LOGIC;
		FILTER_STATE: in filter_type;
		LEDS: out STD_LOGIC_VECTOR(0 to 7)
	);
end LED_INTERFACE ;

architecture Behavioral of LED_INTERFACE  is

Begin
	-- blinking at the camera vsync
	cam_vsync_blinker: process(CAM_VSYNC) is
		variable vsync_count: integer range 0 to 255 := 0;
		variable on_cycle: boolean := false;
	begin
		-- only blink when the camera is actually configured
		if sccb_finished = '1' then
			-- incremet our counter
			if rising_edge(CAM_VSYNC) then
				vsync_count := vsync_count + 1;
				if vsync_count = 30 then
					vsync_count := 0;
					on_cycle := not on_cycle;
				end if;
			end if;
			
			if on_cycle then
				LEDS(cam_vsync_led) <= CAM_VSYNC;
			else
				LEDS(cam_vsync_led) <= '0';
			end if;
		else
			LEDS(cam_vsync_led) <= '0';
		end if;
	end process;
	
	-- blinking at the vga vsync
	vga_vsync_blinker: process(VGA_VSYNC) is
		variable vsync_count: integer range 0 to 255 := 0;
		variable on_cycle: boolean := false;
	begin
		-- only blink when the camera is actually configured
		if sccb_finished = '1' then
			-- incremet our counter
			if rising_edge(VGA_VSYNC) then
				vsync_count := vsync_count + 1;
				if vsync_count = 60 then
					vsync_count := 0;
					on_cycle := not on_cycle;
				end if;
			end if;
			
			if on_cycle then
				LEDS(vga_vsync_led) <= VGA_VSYNC;
			else
				LEDS(vga_vsync_led) <= '0';
			end if;
		else
			LEDS(vga_vsync_led) <= '0';
		end if;
	end process;
	
	-- lighting an LED up when there is no camera configuration
	LEDS(cam_config_led) <= not SCCB_FINISHED;
	
	-- dimmed led for the current state
	filter_state_lighter: process is
		variable clk_count: natural range 0 to 255 := 0;
		variable led_enable: STD_LOGIC := '0';
	begin
		wait until(rising_edge(CLK));
		-- produce the PWM on/off switch
		if clk_count < pwm_percentage_on then
			led_enable := '1';
		else
			led_enable := '0';
		end if;
		
		-- set the led according to the filter state
		case FILTER_STATE is
			when gaussFilter =>
				LEDS(gauss_led) <= led_enable;
				LEDS(scharrX_led) <= '0';
				LEDS(scharrY_led) <= '0';
				LEDS(id_led) <= '0';
			when scharrFilterX =>
				LEDS(gauss_led) <= '0';
				LEDS(scharrX_led) <= led_enable;
				LEDS(scharrY_led) <= '0';
				LEDS(id_led) <= '0';
			when scharrFilterY =>
				LEDS(gauss_led) <= '0';
				LEDS(scharrX_led) <= '0';
				LEDS(scharrY_led) <= led_enable;
				LEDS(id_led) <= '0';
			when idFilter =>
				LEDS(gauss_led) <= '0';
				LEDS(scharrX_led) <= '0';
				LEDS(scharrY_led) <= '0';
				LEDS(id_led) <= led_enable;
			when others => NULL;
		end case;
		
		-- incremet the counter
		clk_count := clk_count + 1;
	end process;
	
end Behavioral;