library ieee;
use ieee.std_logic_1164.all;

entity Y_DATA_EXTRACTOR is
	Port (
		VSYNC: in STD_LOGIC;
		HREF: in STD_LOGIC;
		PCLK: in STD_LOGIC;
		DATA: in std_logic_vector(7 downto 0);
		VSYNC_INTERNAL: out STD_LOGIC;
		HREF_INTERNAL: out STD_LOGIC;
		PCLK_INTERNAL: out STD_LOGIC;
		DATA_INTERNAL: out std_logic_vector(7 downto 0)
	);
end Y_DATA_EXTRACTOR;
architecture Behavioral of Y_DATA_EXTRACTOR is
Begin

-- strip every second pixel
-- Color sequence for the current configuration "00" is: YUYV
-- Exact documentation can be found in the sensor documentation, register 0x3A and 0x3D
readProc: process is
	variable pixel_enable: boolean := false;
	variable HREF_enable: STD_LOGIC := '0';
	variable VSYNC_old: STD_LOGIC;
	variable HREF_old: STD_LOGIC;
begin
	wait until rising_edge(PCLK);
	
	if VSYNC = '1' and VSYNC_old = '0' then
		HREF_enable := '0';
	end if;
	
	-- put the pixel enable to false when HREF goes high
	if HREF = '1' and HREF_old = '0' then
		pixel_enable := false;
		-- switch the HREF enable
		HREF_enable := not HREF_enable;
	end if;
	
	-- half the clock and send the data when enabled
	if pixel_enable then
		DATA_INTERNAL <= DATA;
		PCLK_INTERNAL <= '1';
	else
		PCLK_INTERNAL <= '0';
	end if;
	
	-- switch the enable
	pixel_enable := not pixel_enable;
	
	-- store the current hreF
	HREF_old := HREF;
	VSYNC_old := VSYNC;
	
	-- just pipe the HREF and VSYNC
	HREF_INTERNAL <= HREF and HREF_enable;
	VSYNC_INTERNAL <= VSYNC;
end process;

end Behavioral;