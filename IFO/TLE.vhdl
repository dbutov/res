library ieee;
use ieee.std_logic_1164.all;
use work.fff_types_pack.filter_type;
use ieee.numeric_std.all;

entity TLE is
	Port (
		CLK_USB: in STD_LOGIC;
		CAM_VSYNC: in STD_LOGIC;
		CAM_HREF: in STD_LOGIC;
		CAM_PCLK: in STD_LOGIC;
		CAM_DATA: in std_logic_vector(7 downto 0);
		CAM_XCLK: out STD_LOGIC;
		SDA: inout STD_LOGIC;
		SCL: inout STD_LOGIC;
		BUTTONS: in std_logic_vector(0 to 4);
		VGA_RGB: out std_logic_vector(11 downto 0);
		VGA_VSYNC: out STD_LOGIC;
		VGA_HSYNC: out STD_LOGIC;
		STATUS_LEDS: out std_logic_vector(0 to 7)
	);
end TLE;

architecture TLE_a of TLE is	
	-- input
	-- PLL
	signal CLK_XCLK: STD_LOGIC;
	signal CLK_BUTTON: STD_LOGIC;
	signal CLK_VGA : std_logic;
	signal CLK_I2C_TAP : std_logic;
	
	-- CAM input
	-- communication between y_data_extractor and the button interface
	signal PCLK_unfiltered: STD_LOGIC;
	-- communication between the y_data_extractor and the SCCB configurator
	signal SCCB_FINISHED: STD_LOGIC;
	-- communication between the y_data extractor and the filter + buffer
	signal PCLK_INTERNAL: STD_LOGIC;
	signal VSYNC_INTERNAL: STD_LOGIC;
	signal HREF_INTERNAL: STD_LOGIC;
	signal DATA_INTERNAL: STD_LOGIC_VECTOR(7 downto 0);
	
	-- Buttons
	-- communication between the buttons and the SCCB_CONFIGURATOR
	signal SCCB_TRIGGER: STD_LOGIC;
	-- communication between the buttons and the filters
	signal FILTER_STATE: filter_type;
	signal FILTER_STATE_CHANGED: STD_LOGIC;
	
	-- filter
	
	constant NUM_COL : natural := 320;
	constant NUM_ROW : natural := 240;
	
	signal W_EN				: STD_LOGIC;
	signal W_COL_ADDR 	: natural range 0 to NUM_COL-1;
	signal W_ROW_ADDR		: natural range 0 to NUM_ROW-1;
	signal W_DATA			: STD_LOGIC_VECTOR(5 downto 0);
	signal vsync_cnt 		: STD_LOGIC_VECTOR(8 downto 0);
	signal addrDiag 		:  STD_LOGIC_VECTOR(9 downto 0);
	
	-- output
	signal R_DATA			 : STD_LOGIC_VECTOR(5 downto 0);
	signal R_DATA_PRIME	 : STD_LOGIC_VECTOR(5 downto 0);
	signal R_COL_ADDR 	 : natural range 0 to NUM_COL-1;
	signal R_ROW_ADDR		 : natural range 0 to NUM_ROW-1;
	signal VGA_HS_INTERNAL: std_logic;
	signal VGA_VS_INTERNAL: std_logic;
	signal VGA_RGB_OUT    : STD_LOGIC_VECTOR(11 downto 0);
	signal VGA_RGB_OUT_EN : std_logic;
begin
	-- input
	-- PLL
	pll: entity work.IFO_PLL port map(CLK_USB, CLK_XCLK, CLK_BUTTON, CLK_VGA, CLK_I2C_TAP);
	CAM_XCLK <= CLK_XCLK;
	
	-- CAM input
	pixel_ripper: entity work.Y_data_extractor
		port map(CAM_VSYNC, CAM_HREF, CAM_PCLK, CAM_DATA, VSYNC_INTERNAL, HREF_INTERNAL, PCLK_unfiltered, DATA_INTERNAL);
	-- only output the PCLK if the camera is configured
	PCLK_INTERNAL <= PCLK_unfiltered and SCCB_FINISHED;
	
	-- CAM configuration
	sccb: entity work.SCCB_CONFIGURATOR 
		port map(CLK_XCLK, SCCB_TRIGGER, SDA, SCL, SCCB_FINISHED);
	
	-- Buttons
	buttons_instance: entity work.BUTTON_INTERFACE 
		port map(CLK_BUTTON, BUTTONS, SCCB_TRIGGER, FILTER_STATE, FILTER_STATE_CHANGED);	
		
	-- LED output blinky blinky
	led_server: entity work.LED_INTERFACE
		generic map(5, 6, 7, 0, 1, 2, 3, 20)
		port map(CLK_BUTTON, VGA_VS_INTERNAL, VSYNC_INTERNAL, SCCB_FINISHED, FILTER_STATE, STATUS_LEDS);
	
	-- filter
	filterInstance : entity work.inputBufferAndFilter
		generic map(NUM_COL, NUM_ROW)
		port map( 
			PCLK_INTERNAL, HREF_INTERNAL, VSYNC_INTERNAL, DATA_INTERNAL, FILTER_STATE_CHANGED, FILTER_STATE,
			W_EN, W_COL_ADDR, W_ROW_ADDR, W_DATA);
	
	
	imageBuffer : entity work.dualPortRam
		generic map(NUM_COL, NUM_ROW)
		port map(CLK_XCLK, PCLK_INTERNAL, R_COL_ADDR, R_ROW_ADDR, W_COL_ADDR, W_ROW_ADDR, W_DATA, W_EN, R_DATA_PRIME);
		--port map(CLK_XCLK, PCLK_INTERNAL, R_COL_ADDR, R_ROW_ADDR, W_COL_ADDR, W_ROW_ADDR, W_DATA, '0', R_DATA);
	
	
	-- output
	vga: entity work.vga_out port map (
		hsync => VGA_HS_INTERNAL,
		vsync => VGA_VS_INTERNAL,
		display_time => VGA_RGB_OUT_EN,
		pclock => CLK_VGA,
		row_adr => R_ROW_ADDR,
		col_adr => R_COL_ADDR
	);
	colorTable : entity work.color_lut generic map (
		DATA_WIDTH => 12,
		ADDR_WIDTH => 6
	)
	port map (clk_VGA, to_integer(unsigned(r_data)), VGA_RGB_OUT);
	VGA_HSYNC <= VGA_HS_INTERNAL;
	VGA_VSYNC <= VGA_VS_INTERNAL;
	
	r_data <= std_logic_vector(unsigned(R_DATA_PRIME) + unsigned(addrDiag(5 downto 0)) +unsigned(vsync_cnt(8 downto 3)));
	
	intro: Process		
		variable vsync_old: STD_LOGIC := '0';
	begin
		wait until rising_edge(CLK_VGA);
		if SCCB_FINISHED = '0' then
			addrDiag <= STD_LOGIC_VECTOR(to_unsigned(R_COL_ADDR + R_ROW_ADDR, 10));
			-- wait for a rising edge on vsync
			if vsync_old = '0' and VGA_VS_INTERNAL = '1' then
				vsync_cnt <= STD_LOGIC_VECTOR(unsigned(vsync_cnt) - 1);
			end if;
			vsync_old := VGA_VS_INTERNAL;
		else
			vsync_cnt <= (others =>'0');
			addrDiag <= (others =>'0');
		end if;
	end Process;
	
	VGA_RGB <= VGA_RGB_OUT when VGA_RGB_OUT_EN = '1' else (others => '0');
end architecture TLE_a;
