library ieee;
use ieee.std_logic_1164.all;

entity SCCB_CONFIGURATOR is
	Port (
		I2C_LOGIC_CLK: in STD_LOGIC;
		TRIGGER_CONFIGURATION: in STD_LOGIC;
		SDA: inout STD_LOGIC;
		SCL: inout STD_LOGIC;
		FINISHED: out STD_LOGIC
	);
end SCCB_CONFIGURATOR;

architecture Behavioral of SCCB_CONFIGURATOR is
-- define our i2c component
component i2c_master
  Port(
    clk       : IN     STD_LOGIC;                    --system clock
    reset_n   : IN     STD_LOGIC;                    --active low reset
    ena       : IN     STD_LOGIC;                    --latch in command
    addr      : IN     STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
    rw        : IN     STD_LOGIC;                    --'0' is write, '1' is read
    data_wr   : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
    busy      : OUT    STD_LOGIC;                    --indicates transaction in progress
    data_rd   : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
    ack_error : BUFFER STD_LOGIC;                    --flag if improper acknowledge from slave
    sda       : INOUT  STD_LOGIC;                    --serial data output of i2c bus
    scl       : INOUT  STD_LOGIC);                   --serial clock output of i2c bus
end component;
signal i2c_enable: STD_LOGIC := '0';
signal i2c_addr: STD_LOGIC_VECTOR(6 DOWNTO 0);
signal i2c_rw: STD_LOGIC;
signal i2c_data_wr: STD_LOGIC_VECTOR(7 DOWNTO 0);
signal i2c_busy: STD_LOGIC;
signal i2c_data_rd: STD_LOGIC_VECTOR(7 DOWNTO 0);
signal ack_error: STD_LOGIC;


type i2c_reg_data_type is array (0 to 179, 0 to 1) of std_logic_vector(7 downto 0);
signal i2c_reg_data: i2c_reg_data_type := (
  -- start with the default linux configuration, normally called ov7670_default_regs 
  (x"12", x"80"),
  (x"3a", x"04"),  -- OV 
  (x"12", x"00"),  -- VGA 
  --
   -- Set the hardware window.  These values from OV don't entirely
   -- make sense - hstop is less than hstart.  But they work...
   
  (x"17", x"13"), (x"18", x"01"),
  (x"32", x"b6"), (x"19", x"02"),
  (x"1a", x"7a"),  (x"03", x"0a"),

  (x"0c", x"00"),  (x"3e", x"00"),
  -- Mystery scaling numbers 
  (x"70", x"3a"),   (x"71", x"35"),
  (x"72", x"11"),   (x"73", x"f0"),
  (x"a2", x"01"),  -- 0x02 changed to 1
  (x"15", x"02"),
  -- Gamma curve values 
  (x"7a", x"20"),   (x"7b", x"10"),
  (x"7c", x"1e"),   (x"7d", x"35"),
  (x"7e", x"5a"),   (x"7f", x"69"),
  (x"80", x"76"),   (x"81", x"80"),
  (x"82", x"88"),   (x"83", x"8f"),
  (x"84", x"96"),   (x"85", x"a3"),
  (x"86", x"af"),   (x"87", x"c4"),
  (x"88", x"d7"),   (x"89", x"e8"),
  -- AGC and AEC parameters.  Note we start by disabling those features,
   --  then turn them only after tweaking the values. 
  (x"13", x"C0"),  -- Was: 0x80 | 0x01STEP i.e.: 0x80  0x80  | 0x01STEP  0x40 
  (x"00", x"00"),  (x"10", x"00"),
  (x"0d", x"40"), -- magic reserved bit 
  (x"14", x"18"), -- 4x gain + magic rsvd bit 
  (x"a5", x"05"),  (x"ab", x"07"),
  (x"24", x"95"),  (x"25", x"33"),
  (x"26", x"e3"),  (x"9f", x"78"),
  (x"a0", x"68"), (x"a1", x"03"), -- magic 
  (x"a6", x"d8"), (x"a7", x"d8"),
  (x"a8", x"f0"), (x"a9", x"90"),
  (x"aa", x"94"),
  (x"13", x"C5"), -- Was: 0x80|0x01STEP|0x04|0x01 i.e.:  0x80  0x80  | 0x01STEP  0x40 | 0x04  0x04 | 0x01  0x01 
  (x"30", x"00"),(x"31", x"00"),--disable some delays
  -- Almost all of these are magic "reserved" values.  
  (x"0e", x"61"), (x"0f", x"4b"),
  (x"16", x"02"),   (x"1e", x"07"),
  (x"21", x"02"),   (x"22", x"91"),
  (x"29", x"07"),   (x"33", x"0b"),
  (x"35", x"0b"),   (x"37", x"1d"),
  (x"38", x"71"),   (x"39", x"2a"),
  (x"3c", x"78"),  (x"4d", x"40"),
  (x"4e", x"20"),   (x"69", x"00"), -- 0x69 is 0x69X
  --(x"6b", x"4a"),
  (x"74", x"10"),
  (x"8d", x"4f"),   (x"8e", x"00"),
  (x"8f", x"00"),    (x"90", x"00"),
  (x"91", x"00"),    (x"96", x"00"),
  (x"9a", x"00"),    (x"b0", x"84"),
  (x"b1", x"0c"),   (x"b2", x"0e"),
  (x"b3", x"82"),   (x"b8", x"0a"),

  -- More reserved magic, some of which tweaks white balance 
  (x"43", x"0a"),   (x"44", x"f0"),
  (x"45", x"34"),   (x"46", x"58"),
  (x"47", x"28"),   (x"48", x"3a"),
  (x"59", x"88"),   (x"5a", x"88"),
  (x"5b", x"44"),   (x"5c", x"67"),
  (x"5d", x"49"),   (x"5e", x"0e"),
  (x"6c", x"0a"),   (x"6d", x"55"),
  (x"6e", x"11"),   (x"6f", x"9e"), -- it was 0x9F "9e for advance AWB" 
  (x"6a", x"40"),   (x"01", x"40"),
  (x"02", x"60"),
  (x"13", x"C7"), -- Was: 0x80|0x01STEP|0x04|0x01|0x02 i.e.: 0x80  0x80  | 0x01STEP  0x40 | 0x04  0x04 | 0x01  0x01 | 0x02  0x02

  -- Matrix coefficients 
  (x"4f", x"80"),   (x"50", x"80"),
  (x"51", x"00"),    (x"52", x"22"),
  (x"53", x"5e"),   (x"54", x"80"),
  (x"58", x"9e"),

  (x"41", x"08"), (x"3f", x"00"),
  (x"75", x"05"),   (x"76", x"e1"),
  (x"4c", x"00"),    (x"77", x"01"),
  (x"3d", x"48"),  --Was: 0xc3
  (x"4b", x"09"), 
  (x"c9", x"60"),   --(x"41", x"38"),
  (x"56", x"40"),

  (x"34", x"11"),
  (x"3b", x"12"), -- Was: 0x02|0x10 i.e.:  0x02 0x02 | 0x10 0x10
  (x"a4", x"82"),   --Was 0x88
  (x"96", x"00"),
  (x"97", x"30"),   (x"98", x"20"),
  (x"99", x"30"),   (x"9a", x"84"),
  (x"9b", x"29"),   (x"9c", x"03"),
  (x"9d", x"4c"),   (x"9e", x"3f"),
  (x"78", x"04"),

  -- Extra-weird stuff.  Some sort of multiplexor register 
  (x"79", x"01"),   (x"c8", x"f0"),
  (x"79", x"0f"),   (x"c8", x"00"),
  (x"79", x"10"),   (x"c8", x"7e"),
  (x"79", x"0a"),   (x"c8", x"80"),
  (x"79", x"0b"),   (x"c8", x"01"),
  (x"79", x"0c"),   (x"c8", x"0f"),
  (x"79", x"0d"),   (x"c8", x"20"),
  (x"79", x"09"),   (x"c8", x"80"),
  (x"79", x"02"),   (x"c8", x"c0"),
  (x"79", x"03"),   (x"c8", x"40"),
  (x"79", x"05"),   (x"c8", x"30"),
  (x"79", x"26"),


  -- now the qvga configuration normally called qvga_ov7670 
  (x"3e", x"19"),
  (x"72", x"11"),
  (x"73", x"f1"),
  (x"17", x"16"),
  (x"18", x"04"),
  (x"32", x"24"),
  (x"19", x"02"),
  (x"1a", x"7a"),
  (x"03", x"0a"),


  -- now the yuv configuration, normally called yuv422_ov7670 
  (x"12", x"00"),  -- Selects YUV mode 
  (x"8c", x"00"),  -- No RGB444 please 
  (x"04", x"00"),
  (x"40", x"c0"),
  (x"14", x"6A"), -- 128x gain ceiling; 0x8 is reserved bit 
  (x"4f", x"80"),   -- "matrix coefficient 1" 
  (x"50", x"80"),   -- "matrix coefficient 2" 
  (x"51", x"00"),    -- vb 
  (x"52", x"22"),   -- "matrix coefficient 4" 
  (x"53", x"5e"),   -- "matrix coefficient 5" 
  (x"54", x"80"),   -- "matrix coefficient 6" 
  (x"3d", x"40"),

  
  -- misc configuration 
  (x"15", x"00"),  -- Activate the PCKL on HREF LOW (free running)
  (x"11", x"00"),  -- Let the PCKL run at full speed (divider = 0+1)


  (x"ff", x"ff")  -- END MARKER 
);


Begin

	i2c: i2c_master port map(I2C_LOGIC_CLK, '1', i2c_enable, i2c_addr, i2c_rw, i2c_data_wr, i2c_busy, i2c_data_rd, ack_error, SDA, SCL);
	
QVGAProc: process is
	variable setting_cnt: integer range 0 to 190 := 0;
	variable busy_cnt: integer range 0 to 3 := 0;
	variable busy_prev: STD_LOGIC := '0';
	variable trigger_prev: STD_LOGIC := '0';
	variable send_address: boolean := true;
	variable cam_addr: sTD_LOGIC_VECTOR(6 DOWNTO 0) := "0100001"; -- x"21";
	variable enable: boolean := false;
	variable cfg_finished: STD_LOGIC := '0';
begin
	wait until rising_edge(I2C_LOGIC_CLK);
	-- only start doing stuff on rising edge, to prevent sending the configuration
	-- multiple times pre button press
	if (TRIGGER_CONFIGURATION = '1' and trigger_prev = '0') then
		enable := true;
	end if;
	trigger_prev := TRIGGER_CONFIGURATION;
	if enable then
		-- When we reach x"FF" we stop doing stuff
		--if setting_cnt <= 8 then
		if i2c_reg_data(setting_cnt, 0) /= x"FF" then
			-- make sure to tell the device that we are currently messing with the camera
			cfg_finished := '1';
			-- increment the counter when we have a busy transition
			if(busy_prev = '0' AND i2c_busy = '1') then
				busy_cnt := busy_cnt + 1;
			end if;
			 -- also store the state for the next itteration
			busy_prev := i2c_busy;
			-- talk to the i2c master depending on the current progress of the transaction
			case busy_cnt is
				when 0 =>
					-- init the transmission and send the register address
					i2c_enable <= '1';
					i2c_addr <= cam_addr;
					i2c_rw <= '0'; 
					i2c_data_wr <= i2c_reg_data(setting_cnt, 0); 
				when 1 =>
					-- now we send the register value, the rest stays the same
					i2c_data_wr <= i2c_reg_data(setting_cnt, 1);
				when 2 =>
					-- the I2C bus is now in the process of sending the register value, next we will stop the transmission
					i2c_enable <= '0';
					-- when the I2C bus goes back to busy LOW, we will reset the busy_cnt and select the next address/value pair
					if(i2c_busy = '0') then
						setting_cnt := setting_cnt + 1;
						busy_cnt := 0;
					end if;
				when others =>
					-- this shouldn't happen, so we will reset the busy_cnt and turn off the i2c master just to be sure
					i2c_enable <= '0';
					busy_cnt := 0;
			end case;			
		else
			-- reset to start the transmission again
			enable := false;
			setting_cnt := 0;
			-- also tell the upper entity that we have finished our job
			cfg_finished := '1';
		end if;
	end if;
	FINISHED <= cfg_finished;
end process;
	

end Behavioral;
