library ieee;
use ieee.std_logic_1164.all;
use work.fff_types_pack.all;

entity BUTTON_INTERFACE is
	Port (
		CLK: in STD_LOGIC;
		BUTTONS: in STD_logic_vector(0 to 4);
		SCCB_TRIGGER: out STD_LOGIC;
		FILTER_SETTING: out filter_type;
		FILTER_CHANGED: out STD_LOGIC
	);
end BUTTON_INTERFACE;

architecture Behavioral of BUTTON_INTERFACE is
signal BUTTONS_inverted: STD_logic_vector(0 to 4);
signal BUTTONS_debounced: STD_logic_vector(0 to 4);

-- define components
component DEBOUNCER is
	Port (clk : in STD_LOGIC; rst : in STD_LOGIC; input_sig : in STD_LOGIC; output_sig : out STD_LOGIC);
end component;

Begin
	
	-- Buttons are active LOW, so we invert the signal here
	BUTTONS_inverted <= not BUTTONS;
	
	-- generate one debouncer for each button
	GEN_DEBOUNCER: for i in 0 to 4 generate
      bouncer : DEBOUNCER port map(CLK, '0', BUTTONS_inverted(i), BUTTONS_debounced(i));
   end generate GEN_DEBOUNCER;
	
	-- SCCB output
	SCCB_TRIGGER <= BUTTONS_debounced(4);
	
	-- Extract the filter state from the first three butTONS
	filter_state_proc: process is
		variable current_state: filter_type := gaussFilter;
		variable last_state: filter_type := gaussFilter;
	begin
		wait until rising_edge(CLK);
		-- decode the button input
		case BUTTONS_debounced(0 to 3) is
			when "1000" =>
				current_state := gaussFilter;
			when "0100" =>
				current_state := scharrFilterX;
			when "0010" =>
				current_state := scharrFilterY;
			when "0001" =>
				current_state := idFilter;
			when others => NULL;
		end case;
		-- hand over the current state
		FILTER_SETTING <= current_state;
		-- check if the state has changed
		if (last_state /= current_state) then
			-- send a state changed signal
			FILTER_CHANGED <= '1';
		else
			-- reset the state changed signal
			FilTER_CHANGED <= '0';
		end if;
		-- update the last state
		last_state := current_state;
	end process;
	
end Behavioral;