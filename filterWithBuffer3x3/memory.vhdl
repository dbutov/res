library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use work.fff_types_pack.all;

entity ram is
	generic( size : natural);
	port(
		clk, writeEn 	: in std_logic;
		address			: in natural; 
		pixelIn 			: in std_logic_vector(7 downto 0);
		pixelOut			: out std_logic_vector(7 downto 0)
	);
end entity;

architecture ram_a of ram is 

	signal address_local : natural;
	signal memory : pixelRow_type(0 to size-1);
begin 

	process begin wait until rising_edge(clk);
			
		if writeEn = '1' then 
			memory(address) <= pixelIn;
		end if;
				
		address_local <= address;
	end process;
	
	pixelOut <= memory(address_local);

end ram_a;



library ieee;
use ieee.std_logic_1164.all;

entity dualPortRam is

	generic(	numCol, numRow : natural );
	port 
	(
		rclk	: in std_logic;
		wclk	: in std_logic;
		rColAddr	: in natural range 0 to numCol-1;
		rRowAddr	: in natural range 0 to numRow-1;
		wColAddr	: in natural range 0 to numCol-1;
		wRowAddr	: in natural range 0 to numRow-1;
		data	: in std_logic_vector(5 downto 0);
		we		: in std_logic := '1';
		q		: out std_logic_vector(5 downto 0)
	);

end dualPortRam;

architecture dualPortRam_a of dualPortRam is

	-- Build a 2-D array type for the RAM
	subtype word_t is std_logic_vector(5 downto 0);
	type memory_t is array(0 to numCol*numRow-1) of word_t;

	signal wAddr, rAddr : natural range 0 to numCol*numRow-1;
	
	-- Declare the RAM signal.	
	signal ram : memory_t;

begin

	wAddr <= wRowAddr*numCol + wColAddr;
	rAddr <= rRowAddr*numCol + rColAddr;

	process(wclk)
	begin
	if(rising_edge(wclk)) then 
		if(we = '1') then
			ram(wAddr) <= data;
		end if;
	end if;
	end process;

	process(rclk)
	begin
	if(rising_edge(rclk)) then 
		q <= ram(rAddr);
	end if;
	end process;

end dualPortRam_a;