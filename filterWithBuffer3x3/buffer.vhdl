library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use work.fff_types_pack.all;

entity buffer3Rows is
	generic( numCol, numRow : natural);
	port (
		clk				: in std_logic;
		readEn, reset	: in std_logic := '0';
		pixel				: in std_logic_vector(7 downto 0);
		windowPixel		: out pixel3x3_type
	);
end entity;


architecture buffer3Rows_a of buffer3Rows is

	constant waitForRowTime : natural := 144;
	
	signal pixelTmp 	: std_logic_vector(7 downto 0);
	
	-- start with one for better handling of halo values (others => 0)
	signal colCnt : natural range 0 to numCol-1 := 0;
	signal rowCnt : natural range 0 to numRow := 0;
	signal waitCnt : natural range 0 to waitForRowTime-1 := 0;
	
	signal writeSelect 			: std_logic_vector(0 to 2) := "001" ;
	
	signal lastColOfWindowTmp 	: pixel3_type;
	signal windowTmp 				: pixel3x3_type;
	
	type state_type is ( waitForImage, readRow, waitForNextRow, waitForEmptyRow );
	signal state 	: state_type := waitForImage;
	
	
	component ram is
	generic( size : natural);
	port(
		clk, writeEn 	: in std_logic;
		address			: in natural; 
		pixelIn 			: in std_logic_vector(7 downto 0);
		pixelOut			: out std_logic_vector(7 downto 0)
	);
	end component;
	
	
begin

	pixelTmp <= x"00" when rowCnt = numRow else pixel;

	ram0: ram generic map(size=>numCol) port map(clk, writeSelect(0), colCnt, pixelTmp, lastColOfWindowTmp(0));
	ram1: ram generic map(size=>numCol) port map(clk, writeSelect(1), colCnt, pixelTmp, lastColOfWindowTmp(1));
	ram2: ram generic map(size=>numCol) port map(clk, writeSelect(2), colCnt, pixelTmp, lastColOfWindowTmp(2));
	
	windowTmp(0)(2) <= lastColOfWindowTmp(0) when state = readRow else x"00";
	windowTmp(1)(2) <= lastColOfWindowTmp(1) when state = readRow else x"00";
	windowTmp(2)(2) <= lastColOfWindowTmp(2) when state = readRow else x"00";
	
	with writeSelect select
		windowPixel(0) <= windowTmp(1) when "100",
								windowTmp(2) when "010",
								windowTmp(0) when others;
		
	with writeSelect select
		windowPixel(1) <= windowTmp(2) when "100",
								windowTmp(0) when "010",
								windowTmp(1) when others;
		
	with writeSelect select
		windowPixel(2) <= windowTmp(0) when "100",
								windowTmp(1) when "010",
								windowTmp(2) when others;
							
							
	process begin
		wait until rising_edge(clk);
		
			windowTmp(0)(0) <= windowTmp(0)(1);
			windowTmp(0)(1) <= windowTmp(0)(2);
							
			windowTmp(1)(0) <= windowTmp(1)(1);
			windowTmp(1)(1) <= windowTmp(1)(2);
							
			windowTmp(2)(0) <= windowTmp(2)(1);
			windowTmp(2)(1) <= windowTmp(2)(2);
			
			case state is
				
				when waitForImage =>

					rowCnt <= 0;
					colCnt <= 0;
					waitCnt<= 0;
				
					if( readEn = '1' )then
						state <= readRow;
					end if;
					
				when readRow =>
				
					if( reset = '1' )then 
						
						state <= waitForImage;
					
					else
					
						-- handle row complete
						if( colCnt = numCol-1 )then
						
							colCnt 	<= 0;
							
							case writeSelect is						
								when "001" =>
									writeSelect <= "100";
								when "100" =>
									writeSelect <= "010";
								when others =>
									writeSelect <= "001";
							end case;
							
							if( rowCnt = numRow-1 )then
							
								state <= waitForEmptyRow;
								rowCnt <= rowCnt +1;
							elsif( rowCnt = numRow )then
							
								state <= waitForImage;
								rowCnt <= 1;
							else
							
								state <= waitForNextRow;
								rowCnt <= rowCnt +1;
							end if;
							
						else --colCnt did not reach maximum value
						
							colCnt <= colCnt +1;	
							
						end if;
					
					end if; -- reset
			
			when waitForNextRow =>
		
				if( reset = '1' )then 
						
						state <= waitForImage;	
				elsif( readEn = '1' )then
					
					state <= readRow;
				end if;
		
	
			when waitForEmptyRow =>
		
				if( waitCnt = waitForRowTime-1)then
					waitCnt <= 0;
					state <= readRow;
				else
					waitCnt <= waitCnt+1;
				end if;
			
			end case;
	end process;
	
end buffer3Rows_a;