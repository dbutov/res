library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

package fff_types_pack is

	type 	pixelRow_type			is array (natural range <>) of std_logic_vector(7 downto 0);	
	
	type 	pixel3_type				is array (0 to 2) of std_logic_vector(7 downto 0);
	type 	pixel3x3_type			is array (0 to 2) of pixel3_type;
	
	type 	mask4bit3_type				is array (0 to 2) of unsigned(3 downto 0);
	type 	mask4bit3x3_type			is array (0 to 2) of mask4bit3_type;

--	type 	mask4bit3x3_type			is array (0 to 2, 0 to 2) of unsigned(3 downto 0);
	
	type 	maskS5bit3_type			is array (0 to 2) of signed(5 downto 0);
	type 	maskS5bit3x3_type			is array (0 to 2) of maskS5bit3_type;
	
--	type 	maskS5bit3x3_type			is array (0 to 2, 0 to 2) of signed(5 downto 0);
	
	type u3pixel_cast_type 		is array (0 to 2) of unsigned	(8 downto 0);
	type s3pixel_type 			is array (0 to 2) of signed (7 downto 0);	
	
	type filter_type is ( gaussFilter, scharrFilterX, scharrFilterY );
		
end package fff_types_pack;


package body fff_types_pack is

end package body fff_types_pack;