

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use work.fff_types_pack.all;
use work.all;

entity filterBuffer is
	
	port	( 
				clk				: in 	std_logic;
				hRef				: in 	std_logic;
				vSync				: in 	std_logic;
				inPixel			: in 	std_logic_vector(7 downto 0);
				filterChanged	: in 	std_logic;
				filterState		: in 	filter_type;
				outPixel 		: out std_logic_vector(5 downto 0)
			);
end entity;	

architecture a of filterBuffer is


	constant numCol: natural := 320;
	constant numRow: natural := 240;
	constant wDelay	: natural := 5;
	constant rDelay	: natural := 5;
	
	signal wData : std_logic_vector(5 downto 0);

	
	signal wEn 			: std_logic:= '0';
	signal wColAddr		: natural range 0 to numCol-1;
	signal wRowAddr		: natural range 0 to numRow-1;
	
	
	signal rEn 			: std_logic:= '0';
	signal rColCnt		: natural range 0 to numCol-1;
	signal rRowCnt		: natural range 0 to numRow-1;
	
	signal rSkip 		: std_logic :='0';
	signal rDelayCnt	: natural range 0 to rDelay-1;
	
	signal imageBufferOut : std_logic_vector(5 downto 0);
	
begin


	filterInstance : entity work.inputBufferAndFilter
		generic map(numCol, numRow)
		port map( 
			clk, hRef, vSync, inPixel, filterChanged, filterState,
			wEn, wColAddr, wRowAddr, wData);
			
	
	imageBuffer : entity work.dualPortRam
		generic map(numCol, numRow)
		port map(clk, clk, rColCnt, rRowCnt, wColAddr, wRowAddr, wData, wEn, outPixel);
			
	
	-- set address to read for test case
	process 
	begin
		wait until rising_edge(clk);
		case rEn is	
			when '0' =>
			
				case rDelayCnt is 
				
					when 0 =>
						if hRef='1' then rDelayCnt<=1; end if;
					when rDelay-1 =>	
						rEn <= '1';
						rDelayCnt <= 0;
					when others =>
						rDelayCnt <= rDelayCnt+1;
				end case;
			
			when '1' =>
			
				case rColCnt is 
				
					when numCol-1 =>
						rColCnt 	<= 0;
						rEn 		<= '0';
						
						if rRowCnt = numRow-1 then 
							rRowCnt <= 0;
							rSkip <= '0';
						elsif rSkip='0' then
							rSkip <='1';
						else	
							rRowCnt <= rRowCnt+1;
						end if;
							
					when others => rColCnt <= rColCnt+1;
				end case;
			when others => rEn <= '0';
			end case;
	end process;
	
end a;
