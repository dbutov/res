

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use work.fff_types_pack.all;

entity filterBuffer_tb is end entity;


architecture a of filterBuffer_tb is 

--	constant numCol: natural := 640;
--	constant numRow: natural := 480;

	constant numCol: natural := 320;
	constant numRow: natural := 240;

	signal clk, vSync, hRef	: std_logic := '0';
	signal isValid 			: std_logic;
	signal inputPixel 	: std_logic_vector(7 downto 0) := x"00";
	signal outputPixel 	: std_logic_vector(5 downto 0);
	
	signal setMask 		: std_logic := '0';
	signal filterMaskState : filter_type := gaussFilter;
	
	component filterBuffer is
	port	( 	
				clk				: in 	std_logic;
				hRef				: in 	std_logic;
				vSync				: in 	std_logic;
				inPixel			: in 	std_logic_vector(7 downto 0);
				filterChanged	: in 	std_logic;
				filterState		: in 	filter_type;
				outPixel 		: out std_logic_vector(5 downto 0)
			);
	end component;	

begin

	filterBufferInstance : filterBuffer
		port map(clk, hRef, vSync, inputPixel, setMask, filterMaskState, outputPixel );

	clk_p: process begin
		clk <= '0';
		wait for 5 ps;
		clk <= '1';
		wait for 5 ps;
	end process;

	p: process begin
		
		wait for numCol*10*10 ps;
		vSync <= '1';
		setMask <= '1';
		wait for numCol*3*10 ps;
		vSync <= '0';
		setMask <= '0';
		
		wait for numCol*17*10 ps;
		
		for rows in 0 to numRow-1 loop
			
			hRef <= '1';
			for i in 0 to 7 loop
				inputPixel <= std_logic_vector(to_unsigned(16*i + rows mod 120, 8));
				
				wait for 40*10 ps;
			end loop;
			hRef <= '0';
			inputPixel <= x"00";
			wait for 144*10 ps;
			
		end loop;
	
	end process;
	
	
end a;